package run;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;


public class nanocode {
	private String str_in(String s, int i, int u)	{
		if(i == u) return "";
		
		char e;
		char n;
		String t = (n = s.charAt( i++ )) != (e = (char) 92) ?
					String.valueOf(n)
					: (i != u?
						String.valueOf(s.charAt( i++ ))
						: "");
		
		if(i < u)	{
			do t += (n = s.charAt( i++ )) != e?
					n
					: (i != u? s.charAt( i++ ): "");

			while(i != u);
		}
		return t;
	}

	public void execCode(String[] s)	{
		int l = s.length;
		int p = 0;

		do	{
			int v;
			if((v = (Integer) this.execFunc( s[ p++ ], p, true )) != 0)
				p = v;
		}
		while(p != l);	
	}

	private boolean[] ifs;
	private int ifsCount;
	public HashMap<String, Object> vars;
	private HashMap<String, Integer> namespaces;

	public StringBuffer response = new StringBuffer();
	
	private Object execFunc(String s, int q, boolean su)	{
		int l = s.length();
		int	p = s.indexOf(":");
		String func = s.substring(0, p++);

		if(su == true)	{
			if(this.ifsCount != 0 &&
				this.ifs[ this.ifsCount ] == false &&
				func.equals("else") &&
				func.equals("elseif") &&
				func.equals("butif") &&
				func.equals("fi"))
				return 0;

			else switch(func.hashCode())	{
					case 1252218203: // namespace
						this.namespaces.put( s.substring(p), q);
						return 0;

					case -1240088218: // gotofi
						this.ifsCount--;

					case 3178851: // goto
						int f;
						if((f = this.namespaces.get( s.substring(p) )) != 0)					
							return f;

						else	{
							System.out.println("A namespace '" + s.substring(p) + "' does not exist");
							System.exit(0);
						}
						break;
						
					case -1240088226: // gotofa
						this.ifsCount = 0;
						
						int f1;
						if((f1 = this.namespaces.get( s.substring(p) )) != 0)					
							return f1;

						else	{
							System.out.println("A namespace '" + s.substring(p) + "' does not exist");
							System.exit(0);
						}
						break;
					}
		}


		int iFunc = 0;
		Object[] a = new Object[255];
		int L = 0;
		int b = p;

		if(p < l)	{
			do switch(s.charAt( p++ ))	{
			case (char) 92: // \\
				p += 2;
				break;
	
			case (char) 58: // :
				iFunc++;
				break;
	
			case (char) 44: // ,
				if(iFunc == 0)
					a[ L++ ] = this.str_in(s, b, (b = p) -1);
				break;
	
			case (char) 46: // .
				if(iFunc-- == 1)
					a[ L++ ] = this.execFunc( s.substring(b, (b = p) -1), 0, false );
				break;
	
			case (char) 95: // _
				@SuppressWarnings("unused")
				int cl;
				if((iFunc -= cl = Integer.valueOf( s.substring(p, p + 2) )) == 1)	{
					a[ L++ ] = this.execFunc( s.substring(b, (b = p) -1), 0, false );
					b += 2;
				}
				p += 2;
				break;
			}
			while(p < l);
		}

		a[ L++ ] = iFunc != 0?
					this.execFunc( s.substring(b, p), 0, false )
					: this.str_in(s, b, p);

		if(su == false) return this.funcs(func, a);
		else	{
			this.funcs(func, a);
			return 0;
		}
	}

	@SuppressWarnings("deprecation")
	private Object funcs(String f, Object[] a)	{
		switch(f.hashCode())	{
		case 104431: // int
			String t = a[0].getClass().getSimpleName();
			return t.equals("String")?
					Integer.valueOf( (String) a[0] )
					: ( t.equals("Integer")?
						(Integer) a[0]
						: ( t.equals("Boolean")?
							(((Boolean) a[0]) == true? 1: 0)
							: Math.round((Float) a[0]) ));
			
		case 97526364: // float
			String t1 = a[0].getClass().getSimpleName();
			return t1.equals("String")?
					Float.valueOf( (String) a[0] )
					: ( t1.equals("Integer")?
						((Integer) a[0]) + 0.0
						: ( t1.equals("Boolean")?
							(((Boolean) a[0]) == true? 1.0: 0.0)
							: (Float) a[0] ));

		case -891985903: // string
			String t2 = a[0].getClass().getSimpleName();
			return (t2.equals("Integer") || t2.equals("Double") || t2.equals("Boolean"))?
					a[0].toString()
					: (String) a[0];


		// operators

		case 114251: // sum
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
						(Float) a[0] + (Float) a[1]
						: (Integer) a[0] + (Integer) a[1];

		case 3449687: // prod
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
						(Float) a[0] * (Float) a[1]
						: (Integer) a[0] * (Integer) a[1];

		case 99457: // dif
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
						(Float) a[0] - (Float) a[1]
						: (Integer) a[0] - (Integer) a[1];

		case 3083768: // divi
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
						(Float) a[0] / (Float) a[1]
						: (Integer) a[0] / (Integer) a[1];

		case 96727: // and
			return (Boolean) a[0] && (Boolean) a[1];

		case 3555: // or
			return (Integer) a[0] * (Integer) a[1];

		case 96757556: // equal
			return a[0].equals(a[1]);

		case -1077632281: // nequal
			return a[0].equals(a[1]) == false;


		case -2097775628: // smaller
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
					(Float) a[0] < (Float) a[1]
					: (Integer) a[0] < (Integer) a[1];

		case -1389170092: // bigger
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
					(Float) a[0] > (Float) a[1]
					: (Integer) a[0] > (Integer) a[1];

		case -1622713440: // smallereq
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
					(Float) a[0] <= (Float) a[1]
					: (Integer) a[0] <= (Integer) a[1];	

		case 742373888: // biggereq
			return (a[0].getClass().getSimpleName().equals("Double") || a[1].getClass().getSimpleName().equals("Double"))?
					(Float) a[0] >= (Float) a[1]
					: (Integer) a[0] >= (Integer) a[1];


		// constants

		case 3569038: // true
			return true;

		case 97196323: // false
			return false;	

		case 3392903: // null
			return null;	
			

		// vars

		case 113762: // set
			return this.vars.put( (String) a[0], a[1] );	

		case 83: // S
			return this.vars.get( (String) a[0] );	


		// ifs

		case 3357: // if
			this.ifs[ this.ifsCount++ +1 ] = a[0].equals(true);	
			break;

		case 94105438: // butif
			this.ifs[ this.ifsCount ] = a[0].equals(true);	
			break;
			
		case 3116345: // else
			int x = this.ifsCount;
			return this.ifs[ x ] = this.ifs[ x ] == false;

		case -1300156394: // elseif
			int x1 = this.ifsCount;
			this.ifs[ x1 ] = this.ifs[ x1 ] == false?
				a[0].equals(true)
				: false;
			break;

		case 3267: // fi
			this.ifsCount--;
			break;


		// other funcs

		case -314717969: // println
			System.out.println(a[0]);
			return a[0];			
			
		case -1268779017: // format
			return ncFormat.format(a);

		case 109522647: // sleep
			int time = (Integer) a[0];
			
			if(time > 5000)
				return "EX5409"; // this is an wOS Error Code
				
			try {
				Thread.sleep(time);
			} catch (InterruptedException e2) {}
			break;
		}
		return true;
	}
}
